package Task2;

import java.math.BigDecimal;

public class Task2Main {
    public static void main(String[] args) {

        Person student = new Student("bogdan", "bucuresti", "licenta", 2, new BigDecimal(2000));
        System.out.println(student.toString());
    }
}
