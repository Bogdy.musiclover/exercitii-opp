package Task2;

import java.math.BigDecimal;

public class Student extends Person {
    private String typeOfStudy;
    private int yearOfStudy;
    private BigDecimal price;

    public Student(String name, String address, String typeOfStudy, int yearOfStudy, BigDecimal price) {
        super(name, address);
        this.typeOfStudy = typeOfStudy;
        this.yearOfStudy = yearOfStudy;
        this.price = price;
    }

    public String getTypeOfStudy() {
        return typeOfStudy;
    }

    public void setTypeOfStudy(String typeOfStudy) {
        this.typeOfStudy = typeOfStudy;
    }

    public int getYearOfStudy() {
        return yearOfStudy;
    }

    public void setYearOfStudy(int yearOfStudy) {
        this.yearOfStudy = yearOfStudy;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override       // vizibilitatea metodei nu poate fi restrictionata atunci cand o suprascriem, dar poate fi marita/relaxata
    public String toString() {
        return "Student{" +
                super.toString() + " " +
                "typeOfStudy='" + typeOfStudy + '\'' +
                ", yearOfStudy=" + yearOfStudy +
                ", price=" + price +
                '}';
    }
}
